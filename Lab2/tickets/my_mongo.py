# -*- coding: utf-8 -*-

from pymongo import MongoClient
import models
import xml.etree.ElementTree as ET
from datetime import datetime
from bson.objectid import ObjectId
from bson.code import Code

class DataBase:
    def __init__(self):
        self.client = MongoClient()
        self.db = self.client['tickets_db']
        self.tickets_rare = self.db['tickets_rare']
        self.tickets_route = self.db['tickets_route']
        self.tickets_train = self.db['tickets_train']
        self.tickets_ticket = self.db['tickets_ticket']

    def load_from_xml(self, filename):
        tree = ET.parse("tickets/static/xml/" + filename)
        root = tree.getroot()

        self.db.tickets_rare.delete_many({})
        self.db.tickets_route.delete_many({})
        self.db.tickets_train.delete_many({})
        self.db.tickets_ticket.delete_many({})

        for child in root:
            if child.tag == "trains":
                for train in child:
                    date = train.attrib["date_departure"].split('-')
                    date_dep = datetime(int(date[0]), int(date[1]), int(date[2]))
                    date = train.attrib["date_arrival"].split('-')
                    date_arr = datetime(int(date[0]), int(date[1]), int(date[2]))

                    self.db.tickets_train.insert_one(models.newTrain(
                        train.attrib["num"], date_dep, date_arr, int(train.attrib["count_places"])
                    ))
            elif child.tag == "routes":
                for route in child:
                    self.db.tickets_route.insert_one(models.newRoute(
                        route.attrib["train"], route.attrib["st_from"], route.attrib["st_to"],
                        float(route.attrib["price"])
                    ))
            elif child.tag == "rares":
                for rare in child:
                    self.db.tickets_rare.insert_one(models.newRare(
                        rare.attrib["type"], float(rare.attrib["coef"]), int(rare.attrib["bedding"]),
                        int(rare.attrib["benefits"]), int(rare.attrib["tea"])
                    ))

        print 'Trains = '
        print self.db.tickets_train.count()
        print 'Rares = '
        print self.db.tickets_rare.count()
        print 'Routes = '
        print self.db.tickets_route.count()

    def get_stations_from(self, station):
        st_from = station.split(' ')

        stations = []
        for st in st_from:
            str = ".*" + st + ".*"
            stations.append(self.tickets_route.find({"st_from": { '$regex' : str}}).distinct("st_from"))

        result = []
        for st in stations:
            for s in st:
                print s
                result.append(s)
        return result

    def get_stations_to(self, station):
        st_to = station.split(' ')

        stations = []
        for st in st_to:
            str = ".*" + st + ".*"
            stations.append(self.tickets_route.find({"st_to": {'$regex': str}}).distinct("st_to"))

        result = []
        for st in stations:
            for s in st:
                print s
                result.append(s)
        return result

    def get_trains_by_route(self, st_from, st_to, date_s, date_f):
        start = date_s.split('-')
        gte = datetime(int(start[0]), int(start[1]), int(start[2]))
        finish = date_f.split('-')
        lt = datetime(int(finish[0]), int(finish[1]), int(finish[2]))

        train_num = self.tickets_route.find({"st_from": st_from, "st_to": st_to})
        nums = []
        for t in train_num:
            nums.append(t["train"])

        trains = self.tickets_train.find({"num": {"$in": nums},
                                          "date_departure": {"$gte": gte, "$lte": lt}})

        result = []
        for t in trains:
            print str(t["date_departure"].date())
            result.append([t["num"], str(t["date_departure"].date())])
        return result

    def get_route_price(self, train_num):
        price = self.tickets_route.find_one({"train": train_num.split(',')[0]})

        print price["price"]
        return price["price"]

    def get_price_coef(self, type_c):
        coef = self.tickets_rare.find_one({"type_c": type_c})

        print coef["coef"]
        return coef["coef"]

    def get_rare(self, type_c, bed, ben, tea):
        rare = self.tickets_rare.find_one({"type_c": type_c, "bedding": int(bed), "benefits": int(ben), "tea": int(tea)})

        print rare
        return rare

    def update_train(self, train_num):
        params = train_num.split(',')
        d = params[1].split('-')
        date = datetime(int(d[0]), int(d[1]), int(d[2]))
        train = self.tickets_train.update_one({"num": params[0], "date_departure": date}, {"$inc": {"count_places": -1}})
        train = self.tickets_train.find_one({"num": params[0], "date_departure": date})

        print train
        return train

    def get_train(self, train_num):
        params = train_num.split(',')
        d = params[1].split('-')
        date = datetime(int(d[0]), int(d[1]), int(d[2]))
        train = self.tickets_train.find_one({"num": params[0], "date_departure": date})

        print train
        return train

    def insert_ticket(self, train, rare, carriage, place):
        self.tickets_ticket.insert_one(models.newTicket(train, rare, carriage, place))

        tick = self.tickets_ticket.find()
        for t in tick:
            print t

    def get_all_tickets(self):
        tickets = self.tickets_ticket.find()
        result = []

        for t in tickets:
            route = self.get_route_by_train_num(t["train"]["num"])

            ticket = []
            ticket.append(t["_id"])
            ticket.append(t["train"]["num"])
            ticket.append(str(t["train"]["date_departure"].date()))
            ticket.append(route["st_from"])
            ticket.append(route["st_to"])
            ticket.append(t["carriage"])
            ticket.append(t["place"])

            if t["rare"]["type_c"] == 'r':
                ticket.append("reserved")
            if t["rare"]["type_c"] == 'c':
                ticket.append("coupe")
            if t["rare"]["type_c"] == 'l':
                ticket.append("lux")

            ticket.append("+ bedding" if t["rare"]["bedding"] == 1 else "- bedding")
            ticket.append("+ benefits" if t["rare"]["benefits"] == 1 else "- benefits")
            ticket.append("+ tea" if t["rare"]["tea"] == 1 else "- tea")

            result.append(ticket)

        return result

    def get_route_by_train_num(self, train_num):
        route = self.tickets_route.find_one({"train": train_num})
        return route

    def delete_ticket(self, id):
        self.tickets_ticket.delete_one({"_id": ObjectId(id)})

    def get_ticket_by_id(self, id):
        tickets = self.get_all_tickets()
        result = []
        for t in tickets:
            if ObjectId(id) == ObjectId(t[0]):
                result = t
                # result[2] = "{:%Y-%m-%d}".format(result[2])
                result[0] = str(result[0])
                result[7] = result[7][0]
                result[8] = (1 if result[8][0] == "+" else 0)
                result[9] = (1 if result[9][0] == "+" else 0)
                result[10] = (1 if result[10][0] == "+" else 0)
                break
        return result

    # def get_train_by_id(self, id):
    #     with self.con:
    #         cur = self.con.cursor()
    #         cur.execute("SELECT * FROM tickets_train WHERE id='{0}'"
    #                     .format(id))
    #         return cur.fetchone()
    #
    # def get_rare_by_id(self, id):
    #     with self.con:
    #         cur = self.con.cursor()
    #         cur.execute("SELECT * FROM tickets_rare WHERE id='{0}'"
    #                     .format(id))
    #         return cur.fetchone()
    #

    #
    # def update_ticket(self, id, train_id, rare_id, carriage_num, place_num):
    #     with self.con:
    #         cur = self.con.cursor()
    #         cur.execute(
    #             "UPDATE tickets_ticket SET train_id = '{0}', rare_id = '{1}', carriage_num = '{2}', place_num = '{3}' WHERE id = '{4}'"
    #             .format(train_id, rare_id, carriage_num, place_num, id))
    #         self.con.commit()
    #
    # def search_stations_full_text(self, station):
    #     with self.con:
    #         cur = self.con.cursor()
    #         cur.execute(
    #             "SELECT DISTINCT st_from FROM tickets_route WHERE MATCH(st_from) AGAINST('\"{0}\"' IN BOOLEAN MODE)"
    #             .format(station))
    #
    #         stations = cur.fetchall()
    #         result = []
    #         for s in stations:
    #             print s
    #             result.append(s[0])
    #         return result




# def TestInitMongo():
#     client = MongoClient()
#     db = client.test
#
#     result = db.restaurants.insert_one(
#         {
#             "address": {
#                 "street": "2 Avenue",
#                 "zipcode": "10075",
#                 "building": "1480",
#                 "coord": [-73.9557413, 40.7720266]
#             },
#             "borough": "Manhattan",
#             "cuisine": "Italian",
#             "grades": [
#                 {
#                     "date": datetime.strptime("2014-10-01", "%Y-%m-%d"),
#                     "grade": "A",
#                     "score": 11
#                 },
#                 {
#                     "date": datetime.strptime("2014-01-16", "%Y-%m-%d"),
#                     "grade": "B",
#                     "score": 17
#                 }
#             ],
#             "name": "Vella",
#             "restaurant_id": "41704620"
#         }
#     )
#
#     print result.inserted_id

    def update_ticket(self, id, train, rare, carriage, place):
        self.tickets_ticket.update({"_id": ObjectId(id)},
                                       {"train": train,
                                        "rare": rare,
                                        "carriage": carriage,
                                        "place": place})

    # К-сть квитків на цей день
    def stats_1(self):
        map1 = Code("""
                function() {
                    emit(this.train.date_departure, 1);
                }
        """)

        reduce1 = Code("""
                function(key, values) {
                    var sum = 0;
                    for(var i in values) {
                        sum += values[i];
                    }
                    return sum;
                }
        """)

        s1 = self.tickets_ticket.map_reduce(map1, reduce1, "Tea per day").find()

        print "-"*40
        print "К-сть квитків на цей день\n"
        for s in s1:
            print s
        print "-" * 40

    # К-сть людей, що придбали чи білизну, чи чай на даний шлях
    def stats_2(self):
        map2 = Code("""
            function() {
                if((this.rare.tea == 1 && this.rare.bedding == 0) ||
                    (this.rare.tea == 0 && this.rare.bedding == 1)) {
                    emit(this.train.num, {tea: this.rare.tea, bedding: this.rare.bedding});
                }
                else {
                    emit(this.train.num, 0);
                }
            }
        """)

        reduce2 = Code("""
            function(key, values) {
                var tea = 0;
                var bedding = 0;
                for(var i in values){
                    tea += values[i].tea;
                    bedding += values[i].bedding;
                }
                return tea + bedding
            }
        """)

        # return {tea: tea, bedding: bedding};
        finalize2 = Code("""
            function(key, value) {
                return value.tea + value.bedding;
            }
        """)

        s2 = self.tickets_ticket.map_reduce(map2, reduce2, "Tea or bedding per route").find()

        print "-" * 40
        print "К-сть людей, що придбали чи білизну, чи чай на даний шлях\n"
        for s in s2:
            print s
        print "-" * 40

    # Задана к-сть шляхів, що продали найбільшу к-сть квитків за заданий проміжок часу
    def stats_3(self):
        date1 = datetime(2016, 10, 26)
        date2 = datetime(2016, 10, 27)
        limit = 1
        s3 = self.tickets_ticket.aggregate([{"$project": {"_id": 1,
                                                          "train": "$train.num",
                                                          "dep": "$train.date_departure"}},
                                            {"$match": {"dep": {"$gte": date1}}},
                                            {"$match": {"dep": {"$lte": date2}}},
                                            {"$group": {"tickets": {"$addToSet": "$_id"},
                                                        "_id": "$train"}},
                                            {"$project": {"_id": 1, "tickets": {"$size": "$tickets"}}},
                                            {"$sort": {"tickets": -1}},
                                            {"$limit": limit}
                                            ])

        print "-" * 40
        print "Задана к-сть шляхів, що продали найбільшу к-сть квитків за заданий проміжок часу\n"
        for s in s3:
            print s
        print "-" * 40