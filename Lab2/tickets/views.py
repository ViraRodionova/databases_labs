from django.shortcuts import render_to_response
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from my_mongo import DataBase

# Create your views here.
def load_data(request):
    mdb = DataBase()
    mdb.load_from_xml("data.xml")
    return HttpResponse("Data is loaded")

@csrf_exempt
def index_page(request):
    return render_to_response('index.html')

@csrf_exempt
def get_stations_from(request):
    if request.method == 'POST':
        mdb = DataBase()
        st_from = request.POST.__getitem__('st_from')
        stations = mdb.get_stations_from(st_from)

        return JsonResponse({'st_from': stations})

@csrf_exempt
def get_stations_to(request):
    if request.method == 'POST':
        mdb = DataBase()
        st_to = request.POST.__getitem__('st_to')
        stations = mdb.get_stations_to(st_to)

        return JsonResponse({'st_to': stations})

@csrf_exempt
def search(request):
    if request.method == 'POST':
        st_from = request.POST.__getitem__('st_from_radio')
        st_to = request.POST.__getitem__('st_to_radio')
        date_s = request.POST.__getitem__('date_start')
        date_f = request.POST.__getitem__('date_finish')
        mdb = DataBase()
        TRAINS = mdb.get_trains_by_route(st_from, st_to, date_s, date_f)
        return JsonResponse({'trains': TRAINS})

@csrf_exempt
def calculate_price(request):
    if request.method == 'POST':
        mdb = DataBase()
        train_num = request.POST.__getitem__('train')
        type_c = request.POST.__getitem__('rare_type')
        total_price = mdb.get_route_price(train_num)
        total_price *= mdb.get_price_coef(type_c)

        return JsonResponse({'total': total_price})

@csrf_exempt
def buy_ticket(request):
    if request.method == 'POST':
        train_num = request.POST.__getitem__('train')
        # train = train_num.split(',', 2)
        carriage = request.POST.__getitem__('carriage')
        place = request.POST.__getitem__('place')
        type_r = request.POST.__getitem__('rare_type')
        adds = request.POST.getlist('adds')
        bed = 0
        ben = 0
        tea = 0

        for a in adds:
            if a == 'bed': bed = 1
            elif a == 'ben': ben = 1
            elif a == 'tea': tea = 1

        mdb = DataBase()
        rare = mdb.get_rare(type_r, bed, ben, tea)
        train = mdb.update_train(train_num)

        mdb.insert_ticket(train, rare, carriage, place)
    return render_to_response('index.html')

@csrf_exempt
def get_all_tickets(request):
    mdb = DataBase()
    tickets = mdb.get_all_tickets()

    mdb.stats_1()
    mdb.stats_2()
    mdb.stats_3()

    return render_to_response("all_tickets.html", {'TICKETS': tickets})

@csrf_exempt
def delete_ticket(request):
    mdb = DataBase()
    ticket = request.POST.__getitem__('ticket')
    id = ticket.split('_')
    mdb.delete_ticket(id[0])

    return HttpResponse('Ticket is deleted')

@csrf_exempt
def edit_ticket(request, edit_id):
    mdb = DataBase()
    ticket = mdb.get_ticket_by_id(edit_id)
    return render_to_response("edit_ticket.html", {'ticket': ticket})

@csrf_exempt
def get_ticket_params(request, edit_id):
    mdb = DataBase()
    ticket = mdb.get_ticket_by_id(edit_id)
    return JsonResponse({'ticket': ticket})

@csrf_exempt
def search_trains_edit(request, edit_id):
    st_from = request.POST.__getitem__('st_from_e')
    st_to = request.POST.__getitem__('st_to_e')
    date_dep = request.POST.__getitem__('date_dep_e')

    mdb = DataBase()
    trains = mdb.get_trains_by_route(st_from, st_to, date_dep, date_dep)
    print trains
    return JsonResponse({'trains': trains})

@csrf_exempt
def save_edit(request, edit_id):
    if request.method == "POST":
        train_num = request.POST.__getitem__('train')
        carriage = request.POST.__getitem__('carriage')
        place = request.POST.__getitem__('place')
        type_r = request.POST.__getitem__('rare_type')
        adds = request.POST.getlist('adds')
        bed = 0
        ben = 0
        tea = 0

        for a in adds:
            if a == 'bed':
                bed = 1
            elif a == 'ben':
                ben = 1
            elif a == 'tea':
                tea = 1
        mdb = DataBase()
        rare = mdb.get_rare(type_r, bed, ben, tea)
        train = mdb.get_train(train_num)

        mdb.update_ticket(edit_id, train, rare, carriage, place)

    return HttpResponse("Ticket is updated")

@csrf_exempt
def stations_from_R(request):
    if request.method == 'POST':
        mdb = DataBase()
        st_from = request.POST.__getitem__('st_from')
        stations = mdb.search_stations_full_text(st_from)

        return JsonResponse({'st_from': stations})