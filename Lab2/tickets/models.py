from __future__ import unicode_literals

from django.db import models

# Create your models here.

def newRoute(num, st_from, st_to, price):
    return {
        "train": num,
        "st_from": st_from,
        "st_to": st_to,
        "price": price,
    }

def newTrain(num, date_dep, date_arr, count_places):
    return {
        "num": num,
        "date_departure": date_dep,
        "date_arrival": date_arr,
        "count_places": count_places
    }

def newRare(type_c, coef, bedding, benefits, tea):
    return {
        "type_c": type_c,
        "coef": coef,
        "bedding": bedding,
        "benefits": benefits,
        "tea": tea
    }

def newTicket(train, rare, carriage, place):
    return {
        "train": train,
        "rare": rare,
        "carriage": carriage,
        "place": place
    }