"""Lab2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from tickets import views as t_views

urlpatterns = [
    url(r'^index/get_stations_from/$', t_views.get_stations_from),
    url(r'^index/get_stations_to/$', t_views.get_stations_to),
    url(r'^index/calculate_total/$', t_views.calculate_price),
    url(r'^index/load_data/$', t_views.load_data),
    url(r'^index/buy_ticket/$', t_views.buy_ticket),
    url(r'^index/search/$', t_views.search),
    url(r'^index/stations_from_R/$', t_views.stations_from_R),

    url(r'^index/show/delete_ticket/$', t_views.delete_ticket),
    url(r'^index/show/(?P<edit_id>.+)_edit/save_edit/$', t_views.save_edit),
    url(r'^index/show/(?P<edit_id>.+)_edit/search/$', t_views.search_trains_edit),
    url(r'^index/show/(?P<edit_id>.+)_edit/get_params/$', t_views.get_ticket_params),
    url(r'^index/show/(?P<edit_id>.+)_edit/$', t_views.edit_ticket),
    url(r'^index/show/$', t_views.get_all_tickets),

    url(r'index/$', t_views.index_page),
    url(r'^admin/', admin.site.urls),
]
