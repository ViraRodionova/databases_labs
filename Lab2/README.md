#ЛАБОРАТОРНА РОБОТА №2
з дисципліни «Бази даних на основі XML»

##ТЕМА: **« Розробка Web-орієнтованого додатку з використанням документоорієнтованої СУБД MongoDB »**

Підготувала: студентка групи КП-42

Родіонова Віра Олексіївна

Перевірив:

Петрашенко Андрій Васильович



**Метою роботи** є здобуття практичних навичок створення та обробки бази даних типу NoSQL на прикладі СУБД MongoDB.

Основою розробки є програмні засоби, розроблені у лабораторній роботі No2 частини 1 (ЛРNo2-Ч1) дисципліни «Бази даних» (тема: “Система продажу залізничних квитків”).

**Завдання роботи** полягає у наступному: 

1. Розробити схему бази даних на основі предметної галузі з ЛРNo2-Ч1 у спосіб, що застосовується в СУБД MongoDB.
2. Розробити модуль роботи з базою даних на основі пакету PyMongo.
3. Реалізувати дві операції на вибір із використанням паралельної обробки даних Map/Reduce.
4. Реалізувати обчислення та виведення результату складного агрегативного запиту до бази даних з використанням функції aggregate() сервера MongoDB.

**Функціональні вимоги**

1. Перетворити сутності діаграми «сутність-зв’язок», розробленої в ЛРNo2-Ч1, у структури, прийнятні для обробки pymongo з урахуванням можливості збереження сутностей у середині інших (вкладення сутностей).
2. Забезпечити реалізацію функцій редагування, додавання та вилучення інформації в центральну «сутність», отриману зі схеми «зірка».
3. Забезпечити реалізацію 2-х функцій агрегування даних за допомогою технології Map/Reduce. Функції обрати самостійно з заданої предметної галузі.
4. Забезпечити реалізацію операції агрегування даних за допомогою технології вбудованої у MongoDB функції aggregate(). Операцію обрати самостійно з заданої предметної галузі, але вона має містити засоби групування, сортування, фільтрації та реконструкції масиву ($unwind).

**Тексти функцій**

* Map/Reduce 1

	Кількість квитків на цей день
```
    def stats_1(self):
        map1 = Code("""
                function() {
                    emit(this.train.date_departure, 1);
                }
        """)

        reduce1 = Code("""
                function(key, values) {
                    var sum = 0;
                    for(var i in values) {
                        sum += values[i];
                    }
                    return sum;
                }
        """)

        self.tickets_ticket.map_reduce(map1, reduce1, "Tea per day")
```

* Map/Reduce 2

	Кількість людей, що придбали чи білизну, чи чай на даний шлях
```
    def stats_2(self):
        map2 = Code("""
            function() {
                if((this.rare.tea == 1 && this.rare.bedding == 0) ||
                    (this.rare.tea == 0 && this.rare.bedding == 1)) {
                    emit(this.train.num, {tea: this.rare.tea, bedding: this.rare.bedding});
                }
                else {
                    emit(this.train.num, 0);
                }
            }
        """)

        reduce2 = Code("""
            function(key, values) {
                var tea = 0;
                var bedding = 0;
                for(var i in values){
                    tea += values[i].tea;
                    bedding += values[i].bedding;
                }
		return {tea: tea, bedding: bedding};
            }
        """)

        finalize2 = Code("""
            function(key, value) {
                return value.tea + value.bedding;
            }
        """)

        self.tickets_ticket.map_reduce(map2, reduce2, {out: "Tea or bedding per route", finalize: finalize2})
```

* Aggregate

	Задана к-сть шляхів, що продали найбільшу к-сть квитків за заданий проміжок часу
```
    def stats_3(self):
        date1 = datetime(2016, 10, 26)
        date2 = datetime(2016, 10, 27)
        limit = 1
        self.tickets_ticket.aggregate([{"$project": {"_id": 1,
                                                          "train": "$train.num",
                                                          "dep": "$train.date_departure"}},
                                            {"$match": {"dep": {"$gte": date1}}},
                                            {"$match": {"dep": {"$lte": date2}}},
                                            {"$group": {"tickets": {"$addToSet": "$_id"},
                                                        "_id": "$train"}},
                                            {"$project": {"_id": 1, "tickets": {"$size": "$tickets"}}},
                                            {"$sort": {"tickets": -1}},
                                            {"$limit": limit}
                                            ])
```

**Копії екранних форм**

![Замовлення квитка](ticket.png "Замовлення квитка")

![Всі квитки](table.png "Всі квитки")

![Результат виконання функцій Map/Reduce, Aggregate](map_reduce_aggregate.png "Результат виконання функцій Map/Reduce, Aggregate")
