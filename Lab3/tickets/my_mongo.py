# -*- coding: utf-8 -*-

from pymongo import MongoClient
import models
from datetime import datetime
from bson.objectid import ObjectId
from bson.code import Code
import random
import redis

class DataBase:
    def __init__(self):
        self.client = MongoClient()
        self.db = self.client['tickets_db']
        self.tickets_rare = self.db['tickets_rare']
        self.tickets_route = self.db['tickets_route']
        self.tickets_train = self.db['tickets_train']
        self.tickets_ticket = self.db['tickets_ticket']
        self.red = redis.Redis()

    def get_stations_from(self, station=None):
        stations = []

        if station is None:
            stations = self.tickets_route.find().distinct("st_from")
        else:
            st_from = station.split(' ')

            for st in st_from:
                str = ".*" + st + ".*"
                stations.append(self.tickets_route.find({"st_from": { '$regex' : str}}).distinct("st_from"))

        result = []
        for st in stations:
            if station is None:
                result.append(st)
            else:
                for s in st:
                    print s
                    result.append(s)
        return result

    def get_stations_to(self, station=None):
        stations = []

        if station is None:
            stations = self.tickets_route.find().distinct("st_to")
        else:
            st_to = station.split(' ')

            for st in st_to:
                str = ".*" + st + ".*"
                stations.append(self.tickets_route.find({"st_to": {'$regex': str}}).distinct("st_to"))

        result = []
        for st in stations:
            if station is None:
                result.append(st)
            else:
                for s in st:
                    result.append(s)
        return result

    def get_trains_by_route(self, st_from, st_to, date_s, date_f):
        start = date_s.split('-')
        gte = datetime(int(start[0]), int(start[1]), int(start[2]))
        finish = date_f.split('-')
        lt = datetime(int(finish[0]), int(finish[1]), int(finish[2]))

        train_num = self.tickets_route.find({"st_from": st_from, "st_to": st_to})
        nums = []
        for t in train_num:
            nums.append(t["train"])

        trains = self.tickets_train.find({"num": {"$in": nums},
                                          "date_departure": {"$gte": gte, "$lte": lt}})

        result = []
        for t in trains:
            print str(t["date_departure"].date())
            result.append([t["num"], str(t["date_departure"].date())])
        return result

    def get_route_price(self, train_num):
        price = self.tickets_route.find_one({"train": train_num.split(',')[0]})

        print price["price"]
        return price["price"]

    def get_price_coef(self, type_c):
        coef = self.tickets_rare.find_one({"type_c": type_c})

        print coef["coef"]
        return coef["coef"]

    def get_rare(self, type_c, bed, ben, tea):
        rare = self.tickets_rare.find_one({"type_c": type_c, "bedding": int(bed), "benefits": int(ben), "tea": int(tea)})

        print rare
        return rare

    def update_train(self, train_num):
        params = train_num.split(',')
        d = params[1].split('-')
        date = datetime(int(d[0]), int(d[1]), int(d[2]))
        train = self.tickets_train.find_one({"num": params[0], "date_departure": date})
        if train["count_places"] == 0:
            return None

        train = self.tickets_train.update_one({"num": params[0], "date_departure": date}, {"$inc": {"count_places": -1}})
        train = self.tickets_train.find_one({"num": params[0], "date_departure": date})
        self.db_changed()

        print train
        return train

    def update_train_load(self, train_num, date_dep):
        train = self.tickets_train.find_one({"num": train_num, "date_departure": date_dep})

        if train is None or train["count_places"] == 0:
            return None

        train = self.tickets_train.update_one({"num": train_num, "date_departure": date_dep},
                                              {"$inc": {"count_places": -1}})
        train = self.tickets_train.find_one({"num": train_num, "date_departure": date_dep})

        print train
        return train

    def get_train(self, train_num):
        params = train_num.split(',')
        d = params[1].split('-')
        date = datetime(int(d[0]), int(d[1]), int(d[2]))
        train = self.tickets_train.find_one({"num": params[0], "date_departure": date})

        return train

    def get_trains(self):
        return self.tickets_train.find().distinct('num')

    def get_rares(self):
        return self.tickets_rare.find().distinct('type_c')

    def insert_ticket(self, train, rare, carriage, place):
        self.db_changed()
        self.tickets_ticket.insert_one(models.newTicket(train, rare, carriage, place))

    def get_all_tickets(self, set=None):
        if set is None:
            tickets = self.tickets_ticket.find()
        else:
            tickets = set
        result = []

        for t in tickets:
            route = self.get_route_by_train_num(t["train"]["num"])

            ticket = []
            ticket.append(str(t["_id"]))
            ticket.append(t["train"]["num"])
            ticket.append(str(t["train"]["date_departure"].date()))
            ticket.append(route["st_from"])
            ticket.append(route["st_to"])
            ticket.append(t["carriage"])
            ticket.append(t["place"])

            if t["rare"]["type_c"] == 'r':
                ticket.append("reserved")
            if t["rare"]["type_c"] == 'c':
                ticket.append("coupe")
            if t["rare"]["type_c"] == 'l':
                ticket.append("lux")

            ticket.append("+ bedding" if t["rare"]["bedding"] == 1 else "- bedding")
            ticket.append("+ benefits" if t["rare"]["benefits"] == 1 else "- benefits")
            ticket.append("+ tea" if t["rare"]["tea"] == 1 else "- tea")

            result.append(ticket)

        return result

    def get_route_by_train_num(self, train_num):
        route = self.tickets_route.find_one({"train": train_num})
        return route

    def delete_ticket(self, id):
        self.db_changed()
        self.tickets_ticket.delete_one({"_id": ObjectId(id)})
        return

    def get_ticket_by_id(self, id):
        t = self.tickets_ticket.find_one({"_id": ObjectId(id)})

        route = self.get_route_by_train_num(t["train"]["num"])

        ticket = []
        ticket.append(str(t["_id"]))
        ticket.append(t["train"]["num"])
        ticket.append(str(t["train"]["date_departure"].date()))
        ticket.append(route["st_from"])
        ticket.append(route["st_to"])
        ticket.append(t["carriage"])
        ticket.append(t["place"])
        ticket.append(t["rare"]["type_c"])
        ticket.append(t["rare"]["bedding"])
        ticket.append(t["rare"]["benefits"])
        ticket.append(t["rare"]["tea"])

        return ticket

    def update_ticket(self, id, train, rare, carriage, place):
        self.db_changed()
        self.tickets_ticket.update({"_id": ObjectId(id)},
                                       {"train": train,
                                        "rare": rare,
                                        "carriage": carriage,
                                        "place": place})

    def search_tickets_by_params(self, date, st_from, st_to, carriage, place, rare):
        query = {}

        if st_from != "":
            query["st_from"] = st_from
        if st_to != "":
            query["st_to"] = st_to

        train_nums = self.tickets_route.find(query).distinct("train")

        query = {}

        query["train.num"] = {"$in": train_nums}

        if date != "":
            query["train.date_departure"] = datetime.strptime(date, '%Y-%m-%d')
        if rare != "":
            query["rare.type_c"] = rare
        if carriage != "":
            query["carriage"] = carriage
        if place != "":
            query["place"] = place

        result = self.tickets_ticket.find(query)
        return self.get_all_tickets(result)

    # К-сть квитків на цей день
    def stats_1(self):
        map1 = Code("""
                function() {
                    emit(this.train.date_departure, 1);
                }
        """)

        reduce1 = Code("""
                function(key, values) {
                    var sum = 0;
                    for(var i in values) {
                        sum += values[i];
                    }
                    return sum;
                }
        """)

        s1 = self.tickets_ticket.map_reduce(map1, reduce1, "Tea per day").find()

        print "-"*40
        print "К-сть квитків на цей день\n"
        for s in s1:
            print s
        print "-" * 40

    # К-сть людей, що придбали чи білизну, чи чай на даний шлях
    def stats_2(self):
        map2 = Code("""
            function() {
                if((this.rare.tea == 1 && this.rare.bedding == 0) ||
                    (this.rare.tea == 0 && this.rare.bedding == 1)) {
                    emit(this.train.num, {tea: this.rare.tea, bedding: this.rare.bedding});
                }
                else {
                    emit(this.train.num, 0);
                }
            }
        """)

        reduce2 = Code("""
            function(key, values) {
                var tea = 0;
                var bedding = 0;
                for(var i in values){
                    tea += values[i].tea;
                    bedding += values[i].bedding;
                }
                return tea + bedding
            }
        """)

        # return {tea: tea, bedding: bedding};
        finalize2 = Code("""
            function(key, value) {
                return value.tea + value.bedding;
            }
        """)

        s2 = self.tickets_ticket.map_reduce(map2, reduce2, "Tea or bedding per route").find()

        print "-" * 40
        print "К-сть людей, що придбали чи білизну, чи чай на даний шлях\n"
        for s in s2:
            print s
        print "-" * 40

    # Задана к-сть шляхів, що продали найбільшу к-сть квитків за заданий проміжок часу
    def stats_3(self):
        date1 = datetime(2016, 10, 26)
        date2 = datetime(2016, 10, 27)
        limit = 1
        s3 = self.tickets_ticket.aggregate([{"$project": {"_id": 1,
                                                          "train": "$train.num",
                                                          "dep": "$train.date_departure"}},
                                            {"$match": {"dep": {"$gte": date1}}},
                                            {"$match": {"dep": {"$lte": date2}}},
                                            {"$group": {"tickets": {"$addToSet": "$_id"},
                                                        "_id": "$train"}},
                                            {"$project": {"_id": 1, "tickets": {"$size": "$tickets"}}},
                                            {"$sort": {"tickets": -1}},
                                            {"$limit": limit}
                                            ])

        print "-" * 40
        print "Задана к-сть шляхів, що продали найбільшу к-сть квитків за заданий проміжок часу\n"
        for s in s3:
            print s
        print "-" * 40

    def load_data(self):
        # self.tickets_rare.delete_many({})
        # self.tickets_route.delete_many({})
        # self.tickets_train.delete_many({})
        # self.tickets_ticket.delete_many({})
        #
        # self.load_rares()
        # self.load_routes()
        # self.load_trains()
        # self.load_tick_date()
        # self.db_changed()

        return

    def load_routes(self):
        train_num = ['111O', '141K', '099K', '134P', '122O', '098K']
        st_from = [['Kyiv', 'Kyiv Passajyrskiy', 'Kyiv Moskovskiy', 'Kyiv Volynskiy'],
                   ['Kyiv', 'Kyiv Passajyrskiy', 'Kyiv Moskovskiy', 'Kyiv Volynskiy'],
                   ['Vinnytsa'],
                   ['Kyiv', 'Kyiv Passajyrskiy', 'Kyiv Moskovskiy', 'Kyiv Volynskiy'],
                   ['Vinnytsa'],
                   ['Kyiv', 'Kyiv Passajyrskiy', 'Kyiv Moskovskiy', 'Kyiv Volynskiy']]
        st_to = ['Lviv', 'Kharkiv', 'Kharkiv', 'Lviv', 'Lviv', 'Kharkiv']
        price = [234, 300, 212, 467, 200, 150]

        for i in range(len(train_num)):
            for j in range(len(st_from[i])):
                self.tickets_route.insert_one(models.newRoute(train_num[i], st_from[i][j],
                                                      st_to[i], price[i]))
        return

    def load_trains(self):
        date_dep = datetime(2016, 11, 1)
        end_date = datetime(2017, 11, 1)

        train_num = ['111O', '141K', '099K', '134P', '122O', '098K']
        count_places = [244, 156, 200, 20, 212, 300]

        while date_dep != end_date:
            try:
                date_arr = date_dep.replace(day=date_dep.day + 1)
            except ValueError:
                if date_dep.month == 12:
                    date_arr = datetime(2017, 1, 1)
                else:
                    date_arr = date_dep.replace(month=date_dep.month + 1, day=1)

            for i in range(len(train_num)):
                self.tickets_train.insert_one(models.newTrain(train_num[i], date_dep, date_arr, count_places[i]))
            date_dep = date_arr

        return

    def load_rares(self):
        types = 'rcl'
        coef = [1, 1.5, 2.5]

        for i in range(3):
            for j in range(8):
                bed = j / 4
                ben = j % 4 / 2
                tea = j % 2
                self.tickets_rare.insert_one(models.newRare(types[i], coef[i],
                                                    bed, ben, tea))
        return

    def load_tickets(self):
        types = 'rcl'
        rare = random.randint(0, 24) - 1
        type_r = types[rare / 8]
        bed = rare % 8 / 4
        ben = rare % 4 / 2
        tea = rare % 2

        train_num = ['111O', '141K', '099K', '134P', '122O', '098K']

        month = random.randint(1, 12)

        try:
            date_dep = datetime(2017 - month / 10, month, random.randint(0, 31))
        except ValueError:
            if month == 2:
                date_dep = datetime(2017 - month / 10, month, 28)
            else:
                date_dep = datetime(2017 - month / 10, month, 30)

        train = self.update_train_load(train_num[random.randint(0, 5)], date_dep)

        if train is not None:
            rare = self.get_rare(type_r, bed, ben, tea)
            carriage = random.randint(0, 14)
            place = random.randint(0, 36)
            self.insert_ticket(train, rare, carriage, place)
        return

    def load_tick_date(self):
        train_num = ['111O', '141K', '099K', '134P', '122O', '098K']

        date = datetime(2017, 10, 1)
        date_stop = datetime(2017, 11, 1)

        while date != date_stop:
            if (self.tickets_ticket.count() == 111000): break
            print date
            for i in range((len(train_num))):
                if (self.tickets_ticket.count() == 111000): break
                train = self.tickets_train.find_one({"num": train_num[i], "date_departure": date})

                if train['count_places'] == 0: continue
                count = random.randint(0, train['count_places'])

                for j in range(count):
                    if(self.tickets_ticket.count() == 111000): break
                    train = self.update_train_load(train_num[i], date)

                    if train is not None:
                        types = 'rcl'
                        rare = random.randint(0, 24) - 1
                        type_r = types[rare / 8]
                        bed = rare % 8 / 4
                        ben = rare % 4 / 2
                        tea = rare % 2
                        rare = self.get_rare(type_r, bed, ben, tea)
                        carriage = random.randint(0, 14)
                        place = random.randint(0, 36)
                        self.insert_ticket(train, rare, carriage, place)

            try:
                date = date.replace(day=date.day + 1)
            except ValueError:
                if date.month == 12:
                    date = datetime(2017, 1, 1)
                else:
                    date = date.replace(month=date.month + 1, day=1)

    def db_changed(self):
        self.red.flushdb()

mdb = DataBase()