from django.shortcuts import render_to_response
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from my_mongo import mdb
import json
import time

# Create your views here.
def load_data(request):
    mdb.load_data()
    return HttpResponse("Data is loaded")

@csrf_exempt
def index_page(request):
    return render_to_response('index.html')

@csrf_exempt
def get_stations_from(request):
    if request.method == 'POST':
        st_from = request.POST.__getitem__('st_from')
        stations = mdb.get_stations_from(st_from)

        return JsonResponse({'st_from': stations})

@csrf_exempt
def get_stations_to(request):
    if request.method == 'POST':
        st_to = request.POST.__getitem__('st_to')
        stations = mdb.get_stations_to(st_to)

        return JsonResponse({'st_to': stations})

@csrf_exempt
def search(request):
    if request.method == 'POST':
        st_from = request.POST.__getitem__('st_from_radio')
        st_to = request.POST.__getitem__('st_to_radio')
        date_s = request.POST.__getitem__('date_start')
        date_f = request.POST.__getitem__('date_finish')
        TRAINS = mdb.get_trains_by_route(st_from, st_to, date_s, date_f)
        return JsonResponse({'trains': TRAINS})

@csrf_exempt
def calculate_price(request):
    if request.method == 'POST':
        train_num = request.POST.__getitem__('train')
        type_c = request.POST.__getitem__('rare_type')
        total_price = mdb.get_route_price(train_num)
        total_price *= mdb.get_price_coef(type_c)

        return JsonResponse({'total': total_price})

@csrf_exempt
def buy_ticket(request):
    if request.method == 'POST':
        train_num = request.POST.__getitem__('train')
        # train = train_num.split(',', 2)
        carriage = request.POST.__getitem__('carriage')
        place = request.POST.__getitem__('place')
        type_r = request.POST.__getitem__('rare_type')
        adds = request.POST.getlist('adds')
        bed = 0
        ben = 0
        tea = 0

        for a in adds:
            if a == 'bed': bed = 1
            elif a == 'ben': ben = 1
            elif a == 'tea': tea = 1

        rare = mdb.get_rare(type_r, bed, ben, tea)
        train = mdb.update_train(train_num)

        if train is not None:
            mdb.insert_ticket(train, rare, carriage, place)
    return render_to_response('index.html')

@csrf_exempt
def get_all_tickets(request, tickets=None):
    trains = mdb.get_trains()
    st_from = mdb.get_stations_from()
    st_to = mdb.get_stations_to()
    rares = mdb.get_rares()
    # mdb.stats_1()
    # mdb.stats_2()
    # mdb.stats_3()

    return render_to_response("all_tickets.html", {'TICKETS': tickets, 'trains': trains,
                                                   'st_from': st_from, 'st_to': st_to, 'rares': rares})

@csrf_exempt
def delete_ticket(request):
    ticket = request.POST.__getitem__('ticket')
    id = ticket.split('_')
    mdb.delete_ticket(id[0])

    return HttpResponse('Ticket is deleted')

@csrf_exempt
def edit_ticket(request, edit_id):
    ticket = mdb.get_ticket_by_id(edit_id)
    return render_to_response("edit_ticket.html", {'ticket': ticket})

@csrf_exempt
def get_ticket_params(request, edit_id):
    ticket = mdb.get_ticket_by_id(edit_id)
    return JsonResponse({'ticket': ticket})

@csrf_exempt
def search_trains_edit(request, edit_id):
    st_from = request.POST.__getitem__('st_from_e')
    st_to = request.POST.__getitem__('st_to_e')
    date_dep = request.POST.__getitem__('date_dep_e')

    trains = mdb.get_trains_by_route(st_from, st_to, date_dep, date_dep)
    print trains
    return JsonResponse({'trains': trains})

@csrf_exempt
def save_edit(request, edit_id):
    if request.method == "POST":
        train_num = request.POST.__getitem__('train')
        carriage = request.POST.__getitem__('carriage')
        place = request.POST.__getitem__('place')
        type_r = request.POST.__getitem__('rare_type')
        adds = request.POST.getlist('adds')
        bed = 0
        ben = 0
        tea = 0

        for a in adds:
            if a == 'bed':
                bed = 1
            elif a == 'ben':
                ben = 1
            elif a == 'tea':
                tea = 1

        rare = mdb.get_rare(type_r, bed, ben, tea)
        train = mdb.get_train(train_num)

        mdb.update_ticket(edit_id, train, rare, carriage, place)

    return HttpResponse("Ticket is updated")

@csrf_exempt
def stations_from_R(request):
    if request.method == 'POST':
        st_from = request.POST.__getitem__('st_from')
        stations = mdb.search_stations_full_text(st_from)

        return JsonResponse({'st_from': stations})

@csrf_exempt
def search_by_params(request):
    result = {}

    if request.method == 'POST':
        params = request.POST.__getitem__('params')
        params = json.loads(params)

        key_params = params["date"] + '' \
                     + params["from"] + '' \
                     + params["to"] + '' \
                     + params["carriage"] + '' \
                     + params["place"] + '' \
                     + params["rare"]

        with Profiler() as p:
            print key_params
            if mdb.red.exists(key_params) == 1:
                result = mdb.red.get(key_params)
                result = json.loads(result)
            else:
                tickets = mdb.search_tickets_by_params(params["date"],
                                             params["from"], params["to"],
                                             params["carriage"], params["place"],
                                             params["rare"])
                html = ""
                for item in tickets:
                    html += '<tr class="table-active">'
                    html += '<td>' + item[1] + '</td>'
                    html += '<td>' + item[2] + '</td>'
                    html += '<td>' + item[3] + '</td>'
                    html += '<td>' + item[4] + '</td>'
                    html += '<td>' + str(item[5]) + '</td>'
                    html += '<td>' + str(item[6]) + '</td>'
                    html += '<td>' + item[7] + '</td>'
                    html += '<td>' + item[8] + '<br>' + item[9] + '<br>' + item[10] + '</td>'
                    html += '<td><button name="edit" id="' + item[0] + '_edit" onclick="editBtn(this.id)">Edit</button></td>'
                    html += '<td><button name="delete" id="' + item[0] + '_del" onclick="deleteBtn(this.id)">Delete</button></td>'
                    html += '<tr>'

                result["html"] = html
                result["count"] = len(tickets)

                s = json.dumps(result)
                mdb.red.set(key_params, s)

    return JsonResponse({"result": result})


class Profiler(object):
    def __enter__(self):
        self._startTime = time.time()

    def __exit__(self, type, value, traceback):
        print "Elapsed time: {:.3f} sec".format(time.time() - self._startTime)