#ЛАБОРАТОРНА РОБОТА №1
з дисципліни «Бази даних на основі XML»

##ТЕМА: « Вивчення базових операцій обробки XML-документів »

Підготувала: студентка групи КП-42

Родіонова Віра Олексіївна

Перевірив:

Петрашенко Андрій Васильович



**Метою роботи** є здобуття практичних навичок створення програм, орієнтованих на обробку XML-документів засобами мови Python.

**Завдання роботи** полягає у наступному: 

1. Виконати збір інформації зі сторінок Web-сайту за варіантом. 
2. Виконати аналіз сторінок Web-сайту для подальшої обробки текстової та графічної інформації, розміщеної на ньому. 
3. Реалізувати функціональні можливості згідно вимог, наведених нижче. 

**Функціональні вимоги:**

1. На основі базової адреси Web-сайту виконати обхід наявних сторінок сайту, відокремлюючи текстову та графічну інформацію від тегів HTML.  Пошук вузлів виконувати засобами XPath. Наступну сторінку для аналізу цього ж сайту обрати як одне із гіперпосилань на даній сторінці. Обмежитись аналізом 20 сторінок сайту. Зберегти XML у вигляді файлу. 
2. Виконати аналіз отриманих даних засобами XML згідно варіанту та вивести результати у консольне вікно. Відбір вузлів виконувати засобами XPath.
3. Проаналізувати вміст Web-сторінок інтернет-магазину (див. варіант). Отримати ціну, опис та зображення для 20 товарів з нього за допомогою DOM-парсеру та мови XPath для пошуку відповідних вузлів. Результат записати в XML-файл.
4. Перетворити отриманий XML-файл у XHTML-сторінку за допомогою мови XSLT. Дані подати у вигляді XHTML-таблиці та записати його у файл.

**Варіант**

![Варіант12](variant.jpg)

**Products.xsl**

```
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" doctype-system
="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
  doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" indent="yes" encoding="utf-8" />
<xsl:template match="/">
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/xhtml; charset=utf-8" />
            <title>Laba 1</title>
        </head>
        <body>
            <table>
                <tbody>
                    <xsl:for-each select="data/product">
                        <tr style="margin-bottom: 25px;">
                            <td style="border-bottom: 1px solid #ddd; padding: 15px;">
                                <img style="max-width: 250px"><xsl:attribute name="src"><xsl:value-of select='image' /></xsl:attribute></img>
                            </td>
                            <td style="border-bottom: 1px solid #ddd; padding: 15px;">
                                <h3><xsl:value-of select="title"/></h3>
                            </td>
                            <td style="border-bottom: 1px solid #ddd; padding: 15px;">
                                <span><xsl:value-of select="price"/></span>
                            </td>
                            <td style="border-bottom: 1px solid #ddd; padding: 15px;">
                                <p><xsl:value-of select="description"/></p>
                            </td>
                        </tr>
                    </xsl:for-each>
                </tbody>
            </table>
        </body>
    </html>
</xsl:template>

</xsl:stylesheet>
```

**Product.html (фрагмент)**

```
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/xhtml; charset=utf-8" />
    <title>Laba 1</title>
  </head>
  <body>
    <table>
      <tbody>
        <tr style="margin-bottom: 25px;">
          <td style="border-bottom: 1px solid #ddd; padding: 15px;">
            <img style="max-width: 250px" src="http://www.hozmart.com.ua/63-large_default/naduvnaya-lodka-honda-honwave-t25ae2.jpg" />
          </td>
          <td style="border-bottom: 1px solid #ddd; padding: 15px;">
            <h3>Надувная лодка HONDA HonWave T25AE2, Хонда (T25AE2)</h3>
          </td>
          <td style="border-bottom: 1px solid #ddd; padding: 15px;">
            <span>no price</span>
          </td>
          <td style="border-bottom: 1px solid #ddd; padding: 15px;">
            <p>Тип	лодка надувная;
Общая длинна (см)	250;
Общая ширина (см)	156;
Количество камер	3+киль;
Материал днища	алюминий;
</p>
          </td>
        </tr>
```


