from lxml import html as html
from lxml import etree as ET

data = ET.Element("data")

page = html.parse('http://www.hozmart.com.ua')
category_list = page.xpath('//ul[@class="reset"]/li[contains(a/@href, "28-otdykh-i-turizm")]/div/ul[@class="inner"]/li/ul/li/a/@href')
product_list = []

for category in category_list:
    category_page = html.parse(category)
    product_list.extend(category_page.xpath('//ul[@id="product_list"]/li/div[@class="center_block"]/h3/a/@href'))
    if len(product_list) >= 20: break
product_list = product_list[0:20]

for link in product_list:

    prod_xml = ET.SubElement(data, 'product')

    product = html.parse(link)

    title = product.xpath('//div[@id="primary_block"]/h1').pop().text
    title_xml = ET.SubElement(prod_xml, 'title')
    title_xml.text = title

    image = product.xpath('//div[@id="primary_block"]/div[@id="pb-right-column"]/div[@id="image-block"]/span/img/@src').pop()
    img_xml = ET.SubElement(prod_xml, 'image')
    img_xml.text = image

    price = product.xpath('//div[@id="primary_block"]/div[@class="pb-center-column"]/div[@class="price"]/p/span')

    if len(price) != 0: price = price.pop().text
    else : price = 'no price'

    price_xml = ET.SubElement(prod_xml, 'price')
    price_xml.text = price

    path = '//div[@id="primary_block"]/div[@id="pb-left-column"]/div[@id="short_description_block"]/div/table/tbody/tr'
    desc_table = product.xpath(path)

    description = ""

    if len(desc_table) == 0: description = 'no description'
    else:
        desc1 = product.xpath(path + '/td/strong')
        desc2 = product.xpath(path + '/td')

        i = 0

        for d in desc1:
            if desc1[i].text == None: description += ''
            else: description += desc1[i].text
            description += '\t'
            if desc2[2*i + 1].text == None: description += ''
            else: description += desc2[2*i + 1].text
            description += ';\n'
            i += 1

    decs_xml = ET.SubElement(prod_xml, 'description')
    decs_xml.text = description

tree = ET.ElementTree(data)
tree.write("data-3.xml", pretty_print=True, encoding="UTF-8")