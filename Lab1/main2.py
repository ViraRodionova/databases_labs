from lxml import etree as ET

def selectMidStats(stats) :
    mid_text = 0
    for stat in stats:
        mid_text += stats[stat]['img']

    print mid_text / len(stats)

tree = ET.parse('data-1.xml')
data = tree.getroot()

stats = {}

for page in data:
    stat = {}
    stat['text'] = page.xpath('count(fragment[@type="text"])')
    stat['img'] = page.xpath('count(fragment[@type="image"])')

    stats[page.attrib['url']] = stat

selectMidStats(stats)





